<?php
  /**
  * 
  */
  class Login_model extends CI_Model
  {
  	
  	function __construct()
  	{
  		parent::__construct();
  		
  	}

    public function get_user_cardential(Array $data){
     
       
        $data_string = json_encode($data);
        
        $curl = curl_init('http://smec-group.com:9123/user/login');
        
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
            );
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        
        header('Content-Type: application/json');
        // Send the request
        $result = curl_exec($curl);
        
        
        
        // Free up the resources $curl is using
       
       
        return   $result ;
         
       
    }
    
    
    public function get_session($token){
        
        
        $curl = curl_init('http://smec-group.com:9123/user/access ');
        
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'X-Token: ' . $token)
            );
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string

        header('Content-Type: application/json');
        // Send the request
        $result = curl_exec($curl);
        
        return   $result ;
        
        
    }
    
    
  }
?>