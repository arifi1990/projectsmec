<?php
  class Dashboard extends CI_Controller{
    
        function __construct(){
       	       parent::__construct();
       	       $this->load->library('form_validation');
       	       $this->load->helper('url');
       	       $this->load->model('User_model');

              if(!$this->session->userdata('access')){ 
                  redirect('login');}
                  
               }


          	public function index(){
          	    
          	   $data['token']=$this->session->userdata('token');
          	 
               $this->load->view('dashboard',$data);
          	}

  }
  ?>