<?php
  class Login extends CI_Controller{
    
        function __construct(){
       	       parent::__construct();
       	       $this->load->library('form_validation');
       	       $this->load->helper('url');
       	       $this->load->model('Login_model','login');
           }


  	public function index(){
       $this->load->View('Auth/login');
  	}
    

    public function authenticate(){
        
        $username=$this->input->post('email');
        $pwd=$this->input->post('pwd');
        
       
        
     
        $cardential = ['username' => $username,
                        'password' => md5($pwd)
    	              ];
             $this->form_validation->set_rules('email', 'E-mail', array('required'));
             $this->form_validation->set_rules('pwd', 'Password', array('required', 'min_length[3]'));

             if($this->form_validation->run() ===FALSE){
               $this->load->View('Auth/login');
             }
             else{
             	$jsonData = $this->login->get_user_cardential($cardential);
             	
             	$data=json_Decode($jsonData);
                 
             	if($data->message=="OK") {
             	    
             	    $jsonDataSession=$this->login->get_session($data->token);
             	    $this->session->set_userdata('token', $data->token);
             	    $dataSession=json_Decode($jsonDataSession);
             	    
             	    if($dataSession->message=="OK"){
             	    
             	        $this->session->set_userdata('access', $dataSession->data->access);
             	        
             	        redirect("dashboard/");
             	    }
             	       
               		}
               
               else
               {
                   
               	//$this->session->set_flashdata('msg',"Error! Your cardential is incorrect!");
               	
               	redirect('login/');

               }
             	
             }
    	      
    }

    // Email test
      
       public function sendm(){
        // Please specify your Mail Server - Example: mail.yourdomain.com.
        ini_set("SMTP","smtp.googlemail.com");
        
        // Please specify an SMTP Number 25 and 8889 are valid SMTP Ports.
        ini_set("smtp_port","465");
        
        // Please specify the return address to use
        ini_set('sendmail_from', '');
        $config = array(
                         'protocol' => 'smtp',
                         'smtp_host' => 'ssl://smtp.googlemail.com',
                         'smtp_port' => 465,
                         'smtp_user' =>'' ,
                         'smtp_pass' => '' );

        $this->load->library('email',$config);
        $this->email->set_newline('\r\n');

            $subject = 'This is a test';
            $message = '<p>This message has been sent for testing purposes.</p>';

            // Get full html:
            $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
                <title>' . html_escape($subject) . '</title>
                <style type="text/css">
                    body {
                        font-family: Arial, Verdana, Helvetica, sans-serif;
                        font-size: 16px;
                    }
                </style>
            </head>
            <body>
            ' . $message . '
            </body>
            </html>';
            // Also, for getting full html you may use the following internal method:
            //$body = $this->email->full_html($subject, $message);

            $result = $this->email
                ->from('')
                ->reply_to('')    // Optional, an account where a human being reads.
                ->to('')
                ->subject($subject)
                ->message($body)
                ->send();

        var_dump($result);
        echo '<br />';
        echo $this->email->print_debugger();

        exit;
       }

  } // end of class
?>